function display(arr) {
    var stk = [];
    stk.push(arr[0]);
    var output = [];

    for (var i = 1; i < arr.length; i++) {
        if( stk[stk.length-1] > arr[i]) {
            stk.push(arr[i]);
        }else {
             while(true) {
                 var last = stk.length-1;
                 if( last>=0 && stk[last] < arr[i]) {
                     var j = arr.indexOf(stk.pop());
                     output[j] = arr[i];
                 }else {
                     stk.push(arr[i]);
                     break;
                 }
             }
        }
    }
    while(true) {
      if(stk.length>0) {
          var j = arr.indexOf(stk.pop());
          output[j] = -1;
      }else {
        break;
      }
    }
    console.log(output);
}

display([4,7,2,5,3]);
